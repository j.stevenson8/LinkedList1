#include <stdio.h>
#include <string.h>
#define MYNULL -1

struct node
{
	int data;
	int next;
	int valid;
};

void init(struct node*);

int add(struct node*,int);

void print(struct node*);

int delete(struct node*,int);

int search(struct node*,int);

void releaseNode(struct node*,int);

int getNode(struct node*);
