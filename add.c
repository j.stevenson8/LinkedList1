#include "node.h"

/*The add function adds a data value to the linked list.
 * It returns a 1 if the operation was successful and a 0 if it is not.
 * Reasons for failure include lack of storage in the linked list array and the number already exists in the linked list.*/

int add(struct node*ll,int num)
{	
	int result,next; //result is the variable used to keep track of the the return value of search
       			 //next is used for the index of the next entry of the linked list	
	int curr=ll[0].next,prev=0; //curr is the node currently being looked at and previous is the last one looked at
	result=search(&ll[0],num); //search the linked list for the number

	if(result==0) //if the number was not found, continue with the operation
	{
	
		if(ll[0].next==MYNULL)//if the sentinel node points to NULL, add the new entry at index 1
		{
			ll[1].data=num;
			ll[0].next=1;
			ll[1].valid=1;
			ll[1].next=MYNULL;
			return 1;
		}
		while(1) //at this point, we know the number is not in the array
			 //therefore, we can infinitely loop without fear of our program running forever
		{
			if(ll[curr].data>num) //if the current node's data is greater than the number we wish to add,
					      //we will add the node in between prev and curr
			{
				next=getNode(&ll[0]);
				ll[next].next=ll[prev].next;
				ll[prev].next=next;
				ll[next].valid=1;
				ll[next].data=num;
				return 1;
			}
			else if(ll[curr].next==MYNULL) //if the current node is the last node in the linked list and its data value is less than the number we wish to add,
						       //we will add it to the end of the linked list
			{
				next=getNode(&ll[0]);
				ll[next].next=ll[curr].next;
				ll[curr].next=next;
				ll[next].valid=1;
				ll[next].data=num;
				return 1;
			}
			else //if neither condition is met, we will step forward one node in the linked list and try again
			{
				prev=curr;
				curr=ll[prev].next;
			}
		}			
	}
	else //if the number was found, return from the function with the error value 0
	{
		return 0;
	}
}	
