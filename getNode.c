#include "node.h"
//The getNode function finds an unused node in the linked list and returns the index of this node.

int getNode(struct node* ll)
{
	for(int i=0;i<100;i++) //this cycles through the array until an unused node is found
	{
		if(ll[i].valid==0) //when an unused node is found, return its index
		{
			return i;
		}		
	}
	return MYNULL ; //if the linked list array is full, return MYNULL
}

