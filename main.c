#include "node.h"
/*The main function is where all user input is handled.
 * It gets commands from the reader and implements those 
 * commands by calling the functions delete, add, search, and print.
 * The 'x' command terminates the program.*/

void main()
{
	struct node ll[100]; //creates the linked list
	
	init(&ll[0]); //initializes all entries in the linked list

	char input[100],extra[100]; //input is used to store the direct input from the user 
				    //extra is used to detect extra input from the user
	char command; //command stores the one character command the user wants the program to execute
	int num,result,flag; //num is used to store any number the user wishes to add, delete, or search for in the linked list
			     //result is used to keep track of the number of operations sscanf performs successfully
			     //flag is used to ensure only one command is used per cycle of the while loop
	
	do //this loop continues on forever
	{
		flag=0; 
		printf(">"); //signifies to the user to input a command
		fgets(input,100,stdin);
		result=sscanf(input,"%c%d%s",&command,&num,extra);
		
		if(result==1)
		{
			if(command=='p')
			{
				flag=1;
				print(&ll[0]);
			}
			else if(command=='x')
			{
				flag=1;
				return;
			}
		}
		else if(result==2)
		{
			if(command=='s')
			{
				flag=1;
				result=search(&ll[0],num);
				if(result==1)
				{
					printf("FOUND\n");
				}	
				else if(result==0)
				{
					printf("NOT FOUND\n");
				}
			}
			else if(command=='d')
			{
				flag=1;
				result=delete(&ll[0],num);
				if(result==1)
				{
					printf("Success\n");
				}	
				else if(result==0)
				{
					printf("NODE NOT FOUND\n");
				}
			}
			else if(command=='i')
			{
				flag=1;
				result=add(&ll[0],num);
				if(result==1)
				{
					printf("Success\n");
				}	
				else if(result==0)
				{
					printf("NODE ALREADY IN LIST\n");
				}
				else if(result==-1)
				{
					printf("OUT OF SPACE\n");
				}
			}
		}
		
		if(flag==0)
		{
			printf("Unknown Command\nHere is a summary of valid commands:\n");	
			printf("\ti number\tinsert\n\t\tThe add command adds a number to the list\n\n");
			printf("\td number\tdelete\n\t\tThe delete command deletes a number from the list\n\n");
			printf("\ts number\tsearch\n\t\tThe search command searches the list for a number\n\n");
			printf("\tp\t\tprint\n\t\tThe print command prints out the list\n\n");
		}
	} while(1);
	return;
}

