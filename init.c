#include "node.h"
//The init function initializes the linked list at the start of the program
void init(struct node* a)
{
	a[0].data=42; //sets the sentinel node's data value equal to 42
	a[0].next=MYNULL; //makes the sentinal node point to MYNULL signalling the end of the list
	a[0].valid=1; //sets the valid flag to 1

	for(int i=1;i<100;i++) //this initializes the linked list
	{
		a[i].data=0;
		a[i].next=0;
		a[i].valid=0; 
	}
	return;
}
	
