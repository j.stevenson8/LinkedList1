#include "node.h" 

/*The delete function deletes a data value from the linked list.
 * It will return a 1 if it is successful and a 0 if it is not.
 * The only reason for failure is that the data value was not found.*/

int delete(struct node*ll,int num)
{
	int prev,curr; //previous and current are the indeces of the previous and current node being looked at
	int result; //result is used to store the return value for the search function
	result=search(&ll[0],num); //searches for the number
	if(result==0) //if the number was not found, return 0
	{
		return 0;
	}
	curr=ll[0].next; //set curr to be the current index 
	prev=0; //set prev to be the previous index
	while(ll[curr].data!=num) //while the current node's data is not equal to the number we wish to delete,
				  //step forward in the linked list
	{
		prev=curr;
		curr=ll[prev].next;
	}

	ll[prev].next=ll[curr].next; //when the correct node is found, set the previous node to point to where the current node is pointing
	releaseNode(&ll[0],curr); //sets the valid flag for the deleted node to 0
 	return 1;
}

