#include "node.h"

int search(struct node*ll,int num)
{
	int i=0;
	if(ll[0].next==MYNULL)
	{
		return 0;
	}
	
	do
	{
		i=ll[i].next;	
		if(ll[i].data==num)
		{
			return 1;
		}	
	} while(ll[i].next!=MYNULL);

	return 0;
}
